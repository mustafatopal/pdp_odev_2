/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kisitestapp;

import java.util.Scanner;
import java.io.*;

import mypackage.RastgeleKisi;
import mypackage.MyController;

/**
 *
 * @author camsld
 */
public class KisiTestApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int secim;
        do{
            System.out.println("1- Rastgele Kişi Üret");
            System.out.println("2- Üretilmiş Dosya Kontrol Et");
            System.out.println("3- Çıkış");
            
            System.out.print("Seçim:");
            
            Scanner deger = new Scanner(System.in);
            secim = deger.nextInt();
            
            if(secim == 1){
                System.out.print("\nKaç Kişi:");
                
                int kisi = deger.nextInt();
                
                for(int i=0;i<kisi;i++){
                    RastgeleKisi yeniKisi = new RastgeleKisi();
                    String yKisi = yeniKisi.KisiUret();
                    DosyayaEkle(yKisi);
                    System.out.println(yKisi);
                }
            }
            else if(secim == 2){
                System.out.println("T.C. Kimlik Kontrol");
                for(String item : getTcs()){
                    System.out.println(item);
                }
                
                System.out.println("\nIMEI Kontrol");
                for(String item : getImeiNumbers()){
                    System.out.println(item);
                }
            }
        }
        while(secim != 3);
    }
    
    
    private static void DosyayaEkle(String veri){
        try{
            File file = new File("Kisiler.txt");
            
            if(!file.exists()){
                file.createNewFile();
            }
            
            FileWriter fWriter = new FileWriter(file, true); 
                    
            BufferedWriter bWriter = new BufferedWriter(fWriter);
            bWriter.write(veri + "\n");
            
            bWriter.close();
            fWriter.close();
            
        }
        catch(IOException ex){
            Hata(ex);
        }
    }
    
    private static String[] getTcs(){
        String dosya = "Kisiler.txt";
        
        int satirSayi = satirSayisi(dosya);
        String[] dizi = new String[satirSayi];
        
        try{
            File file = new File(dosya);
            FileReader fReader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(fReader);
            String tc = "";String satir;int indeks=0;
            while((satir = bReader.readLine())!=null){
                for(int i=0;i<satir.length();i++){
                    char c = satir.toCharArray()[i];
                    if(c==' '){
                        break;
                    }
                    else tc+=c;
                }
                MyController controller = new MyController();
                if(controller.tccontrol(tc)) tc+=" Geçerli";
                else tc+=" Geçersiz";
                dizi[indeks]=tc;
                indeks++;
                tc="";
            }
            bReader.close();
            fReader.close();
        }
        catch(IOException ex){
            Hata(ex);
        }
        
        return dizi;
    }
    
    private static String[] getImeiNumbers(){
        String dosya = "Kisiler.txt";
        
        int satirSayi = satirSayisi(dosya);
        String[] dizi = new String[satirSayi];
        
        try{
            File file = new File(dosya);
            FileReader fReader = new FileReader(file);
            
            BufferedReader bReader = new BufferedReader(fReader);
            String imei="";String satir;int indeks=0;
            while((satir=bReader.readLine())!=null){
                for(int i=satir.length()-1;i>=0;i--){
                    char c=satir.toCharArray()[i];
                    if(c==')'){
                        
                    }
                    else if(c=='('){
                        break;
                    }
                    else imei+=c;
                }
                imei = tersCevir(imei);
                MyController controller = new MyController();
                if(controller.emeicontrol(imei))imei+=" Geçerli";
                else imei+=" Geçersiz";
                dizi[indeks]=imei;
                indeks++;
                imei="";
            }
        }
        catch(IOException ex){
            Hata(ex);
        }
        
        return dizi;
    }
    
    private static int satirSayisi(String dosya){
       int satir = 0;
       
       try{
        File file = new File(dosya);
        
        if(!file.exists()){
                file.createNewFile();
        }
        
        FileReader fileReader = new FileReader(file);

        BufferedReader bReader = new BufferedReader(fileReader);

        while(bReader.readLine()!=null){
            satir++;
        }

        bReader.close();
        fileReader.close();
       }
       catch(IOException ex){
           Hata(ex);
       }
       return satir;
   }
    
    private static void Hata(IOException ex){
        System.out.println("Hata:\n"+ ex.getMessage());
    }
    
    private static String tersCevir(String str){
        String ters = "";
        for(int i=str.length()-1;i>=0;i--){
            ters+=str.toCharArray()[i];
        }
        return ters;
    }
}
