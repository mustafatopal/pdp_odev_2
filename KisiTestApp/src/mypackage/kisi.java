/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

import java.util.Random;
import java.io.*;

/**
 *
 * @author Muhammet
 */
public class kisi {
        
   private String name;

   
   
   public kisi() {
       String dosya = "random_isimler.txt";
       
       Random rnd = new Random();
       try{
        int kisiIndex = rnd.nextInt(satirSayisi(dosya));
       
        File file = new File(dosya);
        FileReader fileReader = new FileReader(file);
       
        BufferedReader bReader = new BufferedReader(fileReader);
       
        String satir;
        int satirIndex = 0;
       
        while((satir = bReader.readLine()) != null){
           if(satirIndex == kisiIndex){
               name = satir;
               break;
           }
           satirIndex++;
        }
       
        bReader.close();
        fileReader.close();
       }
       catch(IOException ex){
           System.out.println("Hata:\n" + ex.getMessage());
       }
   }
   
   private int satirSayisi(String dosya)throws IOException{
       
       File file = new File(dosya);
       FileReader fileReader = new FileReader(file);
       
       BufferedReader bReader = new BufferedReader(fileReader);
       int satir = 0;
       while(bReader.readLine()!=null){
           satir++;
       }
       
       bReader.close();
       fileReader.close();
       
       return satir;
   }

    
    public String getname(){
    
    return this.name;
    }
    
    
    
}
